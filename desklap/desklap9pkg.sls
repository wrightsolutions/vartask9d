
base1manualB_install:
  pkg.installed:
    - pkgs:
      - bzip2


base1manualC_install:
  pkg.installed:
    - pkgs:
      - calendar-google-provider
      - console-setup
      - coreutils
      - cpio
      - cron


base1manualD_install:
  pkg.installed:
    - pkgs:
      - dash
      - diffutils
      - dpkg


base1manualE_install:
  pkg.installed:
    - pkgs:
      - e2fsprogs
      - enigmail


base1manualF_install:
  pkg.installed:
    - pkgs:
      - file
      - findutils
      - fonts-lyx
      - fsarchiver


base1manualG_install:
  pkg.installed:
    - pkgs:
      - gawk
      - gcc
      - geany
      - geany-plugin-xmlsnippets
      - gvfs
      - gzip


base1manualH_install:
  pkg.installed:
    - pkgs:
      - hdparm
      - hostname


base1manualI_install:
  pkg.installed:
    - pkgs:
      - iptables
      - iputils-ping
      - isc-dhcp-client


base1manualK_install:
  pkg.installed:
    - pkgs:
      - keyboard-configuration


base1manualL_install:
  pkg.installed:
    - pkgs:
      - less
      - libaudit-common
      - libc-bin
      - libpam-runtime
      - libsemanage-common
      - lightning
      - login
      - logrotate
      - lsb-base
      - lsof
      - lvm2


base1manualT_install:
  pkg.installed:
    - pkgs:
      - tar
      - telnet
      - thunderbird
      - traceroute
      - tzdata


base1manualW_install:
  pkg.installed:
    - pkgs:
      - wget
      - whiptail
      - wireless-tools
      - wpasupplicant


base1manualX_install:
  pkg.installed:
    - pkgs:
      - xxd
      - xz-utils


base1manualZ_install:
  pkg.installed:
    - pkgs:
      - zile

